## Overview: Application - Static Pages

Congratulations on your new role as the lead software developer at the  dealership. As a warm-up task, you need to build and deploy an initial Django app on IBM Cloud. 

The Django app will be mainly used for user management and authentication, managing car models and makes, and routing other IBM cloud services for dealerships and reviews. You will build this Django app and related cloud services incrementally along the capstone course.

In this learning module, you are asked to perform the following tasks:

- Fork Github repo containing the project template

- Create your own Github repo storing your project assets

- Add a navigation to the website using bootstrap

- Add a "about us" static page

- Add a "contact us" static page

- Deploy the app to IBM Cloud Foundry


Follow the instructional lab to complete the above tasks step by step.
