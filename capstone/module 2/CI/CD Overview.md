## CI/CD Overview

Congratulations on deploying the application to IBM Cloud. The next step is setting up Continuous Integration and Continuous Delivery for your source code. This is particularly important if you have multiple people working on the project. Continuous Integration provides a way for developers to collaborate and Continuous Delivery provides a way to delivery your changes to the clients without interruptions. 

You will learn about Github Actions in this lab. Github Actions let you automate, customize, and execute software development workflows. GitHub Actions are driven by events. You can have them run when somebody opens a pull request or submits new code. There are several other types of events. You can learn more in their documentation.

In this module you will:


- enable Github actions in your forked repo

- create an action that listens to every push request to the main branch and

- create a new build on IBM CloudFoundry when the even occurs

Follow the instructional lab to complete above tasks step by step.
