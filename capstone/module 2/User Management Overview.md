## User Management Overview

Now that you have the initial Django app built and deployed. As the next step, the admins of the dealership review app will be able to identify users and manage their accesses based on roles  (such as anonymous users or registered users). Thus, you are planning to add authentication and authorization, i.e., user management, to the app.

In this lesson, you need to perform the following tasks to add the user management feature:


- Create a super user for the Django admin site

- Add a user login/logout and signup menu items to the navigation bar in the Django template

- Add a Django login view to handle login request

- Add a Django logout view to handle logout request

- Add a Django signup template

- Add a Django signup view to handle signup request

Follow the instructional lab to complete the above tasks step by step.
