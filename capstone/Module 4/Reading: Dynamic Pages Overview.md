## Reading: Dynamic Pages Overview

You created all necessary backend services (Django views and cloud functions) for managing dealerships, reviews, and cars in the last module. Next, it is time to create some stylized front-end Django templates to present those service results to the end users. In this learning module, you need to perform the following tasks to add the front-end to the app:

- Create a dealership list template and update the dealership list view

- Create a dealer details/reviews template and update the dealership detail view

- Create a review submission page and add a submission view

Follow the instructional lab to complete above tasks step by step.
