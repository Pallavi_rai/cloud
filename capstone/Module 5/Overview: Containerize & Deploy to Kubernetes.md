## Overview: Containerize & Deploy to Kubernetes

In line with the latest trends in technology and to avoid vendor lock in, your management team is looking to deploy the dealership application to multiple clouds. The application is currently running on Cloud Foundry, but you have been told not all cloud providers have a hosted Cloud Foundry service. You are put in charge to look at containers as a possible way to mitigate this problem as all the big cloud providers have a way to host and manage containers. When containerizing an application, the process includes packaging an application with its relevant environment variables, configuration files, libraries, and software dependencies. The result is a container image that can then be run on a container platform.  You are also asked to use Kubernetes to manage the containerized deployment. Kubernetes is an open source container orchestration platform that automates deployment, management, and scaling of applications.

In this module you will:

- add ability to your application to run in a container

- add deployment artifacts for your application so it can be managed by Kubernetes

Follow the instructional lab to complete the above tasks step by step.
