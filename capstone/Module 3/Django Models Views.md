## Django Models Views

Now that you have created dealership and views related CRUD cloud functions.  Next, we need to create data models and services for the dealers' inventory. Each dealer manages a car inventory with different car models and makes, which are, in fact, relatively static data, thus suitable to be stored in Django locally. 

To integrate external dealer and review data, you will need to call the cloud function APIs from the Django app and process the API results in Django views. Such Django views can be seen as proxy services to the end user because they fetch data from external resources per users' requests.

In this lesson, you need to perform the following tasks to add car model and make related models and views, and proxy services:

- Create CarModel and CarMake Django models

- Register CarModel and CarMake models with the admin site

- Create new car models objects with associated car makes and dealerships

- Create a `get_dealerships` Django view to get dealer list

- Create a Django `get_dealer_details` view to get reviews of a dealer

- Update the `get_dealer_details` view to call Watson NLU for analyzing review sentiment

- Create an `add_review` Django view to post dealer review

Follow the instructional lab to complete the above tasks step by step.
