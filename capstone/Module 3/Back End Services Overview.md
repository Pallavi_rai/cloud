## Back End Services Overview

The Django application you created in the last module needs to communicate with the database. In this module, you will create actions on IBM Cloud Functions and serve them behind an API endpoint. 

You will build several actions in Python and JavaScript to perform database operations including:

- Get all dealerships

- Get all dealerships for a given state

- Get all reviews for a dealership

- Post a review for a dealership

Follow the instructional lab to complete the above tasks step by step.
