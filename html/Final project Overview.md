## Final Project Overview

Now that you have been equipped with the skills to use HTML, CSS, and JavaScript, you will have the opportunity to practice and create a web application.

In this scenario, you are a web developer for an NGO. Everyone Can Get Rich is a non-governmental charity organization. This NGO works to improve the financial literacy of the common people. They have hired a developer to create an online application. This app titled Simple Interest Calculator, will compute the interest one can earn given the amount you wish to save, the number of years you can keep it with the bank and the interest rate the bank would pay you.

The NGO is very particular about the way the app looks including the colors and the alignment of the form elements.

This project will be graded by your peers who are also completing the course during the same session. This project is worth 20 marks of your total grade, and is distributed as follows:

## Review Criteria

1. Basic Functionality  (6 marks)

2. Principal Amount input box validation (3 marks )

3. How close is the app to the one given in the specification visually? (6 marks)

4. Correct use of HTML5 (3 marks)

5. All styling must be in the CSS file only. No inline or internal stylesheets allowed. (1 marks)

6. All code must be in the JavaScript file only. (1 marks)

## Overview of Assignment Instructions

### Step A: Download the initial project

1. Fork the project into your GitHub account.

2. Clone it onto the Theia lab environment.

### Step B: Finish coding the project

1. Complete the project as per requirements.

2. Push it to GitHub.

3. Keep the GitHub link to your project.

### Submit your work for grading 

Paste the GitHub link of your project when prompted in the peer review section of the final project.
